package com.example.proyectoteamjulio;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity2 extends AppCompatActivity {
    private TextView txtc1,txtc2,txtc3,txtc4,txtc5;
    private EditText edtn1,edtn2,edtn3,edtn4,edtn5;
    private ImageButton btn1,btn2,btn3,btn4,btn5;
    private final int PHONE_CALL_CODE = 100;
    private RequestQueue mQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        txtc1 = findViewById(R.id.txtcompu1);
        txtc2 = findViewById(R.id.txtcompu2);
        txtc3 = findViewById(R.id.txtcompu3);
        txtc4 = findViewById(R.id.txtcompu4);
        txtc5 = findViewById(R.id.txtcompu5);

        edtn1 = findViewById(R.id.edtnumero1);
        edtn2 = findViewById(R.id.edtnumero2);
        edtn3 = findViewById(R.id.edtnumero3);
        edtn4 = findViewById(R.id.edtnumero4);
        edtn5 = findViewById(R.id.edtnumero5);

        btn1=(ImageButton) findViewById(R.id.btnnumero1);
        btn2=(ImageButton) findViewById(R.id.btnnumero2);
        btn3=(ImageButton) findViewById(R.id.btnnumero3);
        btn4=(ImageButton) findViewById(R.id.btnnumero4);
        btn5=(ImageButton) findViewById(R.id.btnnumero5);

        Button btnresult= findViewById(R.id.btndatos);
        mQueue = Volley.newRequestQueue(this);
        btnresult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jsonPeticion();
                btnresult.setEnabled(false);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = edtn1.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }
            private  void versionesAnteriores(String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = edtn2.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }
            private  void versionesAnteriores(String num){
                Intent intentLlamada2 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada2);
                }else{
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = edtn3.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }
            private  void versionesAnteriores(String num){
                Intent intentLlamada3 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada3);
                }else{
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = edtn4.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }
            private  void versionesAnteriores(String num){
                Intent intentLlamada4 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada4);
                }else{
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = edtn5.getText().toString();
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }
            private  void versionesAnteriores(String num){
                Intent intentLlamada5 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada5);
                }else{
                    Toast.makeText(MainActivity2.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result= grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result== PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = edtn1.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));

                        String phoneNumber2 = edtn2.getText().toString();
                        Intent llamada2 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber2));

                        String phoneNumber3 = edtn3.getText().toString();
                        Intent llamada3 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber3));

                        String phoneNumber4 = edtn4.getText().toString();
                        Intent llamada4 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber4));

                        String phoneNumber5 = edtn5.getText().toString();
                        Intent llamada5 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber5));

                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada);
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada2);
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada3);
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada4);
                        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada5);
                    }else{
                        Toast.makeText(this,"No aceptaste el permiso",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        }
    }

    private  boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }

    private void jsonPeticion(){
        String url = "https://my-json-server.typicode.com/Julio170600/Api/db";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("computadoras");
                                JSONObject com1 = jsonArray.getJSONObject(0);
                                String Tipo = com1.getString("Tipo");
                                String Nombre = com1.getString("Nombre");
                                String Procesador = com1.getString("Procesador");
                                String Almacenamiento = com1.getString("Almacenamiento");
                                String Ram = com1.getString("Ram");
                                String Grafica = com1.getString("Grafica");
                                String Numero = com1.getString("Numero");

                                JSONObject com2 = jsonArray.getJSONObject(1);
                                String Tipo2 = com2.getString("Tipo");
                                String Nombre2 = com2.getString("Nombre");
                                String Procesador2 = com2.getString("Procesador");
                                String Almacenamiento2 = com2.getString("Almacenamiento");
                                String Ram2 = com2.getString("Ram");
                                String Grafica2 = com2.getString("Grafica");
                                String Numero2 = com2.getString("Numero");

                                JSONObject com3 = jsonArray.getJSONObject(2);
                                String Tipo3 = com3.getString("Tipo");
                                String Nombre3= com3.getString("Nombre");
                                String Procesador3 = com3.getString("Procesador");
                                String Almacenamiento3 = com3.getString("Almacenamiento");
                                String Ram3 = com3.getString("Ram");
                                String Grafica3 = com3.getString("Grafica");
                                String Numero3 = com3.getString("Numero");

                                JSONObject com4 = jsonArray.getJSONObject(3);
                                String Tipo4 = com4.getString("Tipo");
                                String Nombre4 = com4.getString("Nombre");
                                String Procesador4 = com4.getString("Procesador");
                                String Almacenamiento4 = com4.getString("Almacenamiento");
                                String Ram4 = com4.getString("Ram");
                                String Grafica4 = com4.getString("Grafica");
                                String Numero4 = com4.getString("Numero");

                                JSONObject com5 = jsonArray.getJSONObject(4);
                                String Tipo5 = com5.getString("Tipo");
                                String Nombre5 = com5.getString("Nombre");
                                String Procesador5 = com5.getString("Procesador");
                                String Almacenamiento5 = com5.getString("Almacenamiento");
                                String Ram5 = com5.getString("Ram");
                                String Grafica5 = com5.getString("Grafica");
                                String Numero5 = com5.getString("Numero");



                                txtc1.append("\n"+Tipo + ", \n"+ Nombre +", \n"+ Procesador+ ", \n"+Almacenamiento + ", \n"+ Ram +", \n"+ Grafica);
                                edtn1.append(Numero);

                                txtc2.append(Tipo2 + ", \n"+ Nombre2 +", \n"+ Procesador2+ ", \n"+Almacenamiento2 + ", \n"+ Ram2 +", \n"+ Grafica2);
                                edtn2.append(Numero2);

                                txtc3.append(Tipo3 + ", \n"+ Nombre3 +", \n"+ Procesador3+ ", \n"+Almacenamiento3 + ", \n"+ Ram3 +", \n"+ Grafica3);
                                edtn3.append(Numero3);

                                txtc4.append(Tipo4 + ", \n"+ Nombre4 +", \n"+ Procesador4+ ", \n"+Almacenamiento4 + ", \n"+ Ram4 +", \n"+ Grafica4);
                                edtn4.append(Numero4);

                                txtc5.append(Tipo5 + ", \n"+ Nombre5 +", \n"+ Procesador5+ ", \n"+Almacenamiento5 + ", \n"+ Ram5 +", \n"+ Grafica5);
                                edtn5.append(Numero5);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}